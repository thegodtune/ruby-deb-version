#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: deb_version 1.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "deb_version".freeze
  s.version = "1.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "changelog_uri" => "https://github.com/captn3m0/ruby-deb-version/blob/main/README.md", "homepage_uri" => "https://github.com/captn3m0/ruby-deb-version", "rubygems_mfa_required" => "true", "source_code_uri" => "https://github.com/captn3m0/ruby-deb-version" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Nemo".freeze]
  s.date = "2023-05-30"
  s.email = ["rubygem@captnemo.in".freeze]
  s.files = ["CHANGELOG.md".freeze, "LICENSE.txt".freeze, "README.md".freeze, "bin/console".freeze, "bin/setup".freeze, "lib/deb_version.rb".freeze, "lib/deb_version/compare.rb".freeze, "lib/deb_version/version.rb".freeze]
  s.homepage = "https://github.com/captn3m0/ruby-deb-version".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 3.0.0".freeze)
  s.rubygems_version = "3.4.20".freeze
  s.summary = "A port of Debian Version comparison to Ruby.".freeze
end
